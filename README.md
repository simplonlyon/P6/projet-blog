# Projet Blog PHP

Créer un blog en PHP en utilisant le framework Symfony 4.1

* Modéliser l'application en UML
    * Faire un diagramme de Use case pour l'application
    * Faire un diagramme de Classe pour représenter les entités de l'appli
    * Faire un diagramme d'activité pour représenter l'enchaînement des pages
* Maquetter l'application
    * Faire les wireframes de l'application
    * Faire une maquette basique de l'application
* Créer un projet Symfony et coder l'application avec le framework
* Utiliser Mariadb / PDO pour les bases de données (pas d'ORM)
    * Créer la base de donnée du projet et les tables correspondantes aux entités
    * Créer des classes d'accès au données en utilisant PDO



## Instructions pour la mise en ligne du projet blog sur simplonlyon.fr :
I. Côté local (sur votre machine à vous)
1. S'assurer que ça fonctionne bien comme il faut, sur toutes les pages, en local
2. Faire un export de toute votre base de données avec MySQL Workbench en cochant la case  self-contained file (et décocher "include create schema"). Mettre le .sql obtenu à la racine du dossier de votre projet.
3. Faire un commit et un push de tous vos trucs
4. Se connecter en ssh à simplonlyon.fr (`ssh username@simplonlyon.fr`)

II.Côté Serveur (sur le terminal connecté en SSH)
1. Aller dans votre dossier www
2. Faire un gitclone de votre projet
3. Renommer le dossier obtenu par le clone en "blog" (important, sinon ça ne marchera pas)
4. Aller dans le dossier blog
5. Faire un composer install
6. Se connecter au serveur SQL de simplonlyon avec la commande : `mysql -u username -p`               Votre username est votre nom d'utilisateur.ice simplonlyon et votre mot de passe également
7. Créer la base de donnée avec la commande : `CREATE DATABASE username_blog;`                Remplacez username par votre nom d'utilisateur.ice simplonlyon
8. Quitter mysql avec la commande : `exit`
9. Importer la base de donnée avec la commande :  `mysql -u username -p username_blog < nom_fichier.sql`              Remplacez username par votre nom d'utilisateur.ice simplonlyon et nom_fichier par le nom de votre fichier SQL généré précédemment
10. Créer un fichier .env avec nano et dedans mettre :  
`MYSQL_HOST=localhost
MYSQL_USER=username
MYSQL_PASSWORD=username
MYSQL_DATABASE=username_blog`  
En remplaçant bien à chaque fois username par votre nom d'utilisateur.ice simplonlyon
11. Essayez d'accéder à votre projet voir si ça marche bien pour toutes les pages/formulaires
12. Si oui, rajouter dans le .env la ligne : `APP_ENV=prod`
